package si.uni_lj.fri.pbd.miniapp3.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.io.InputStream;
import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;
import si.uni_lj.fri.pbd.miniapp3.ui.DetailsActivity;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<RecipeSummaryIM> recipes;
    private LayoutInflater mInflater;
    private boolean fromSearch;


    public RecyclerViewAdapter(Context context, List<RecipeSummaryIM> recipes, boolean fromSearch) {
        this.mInflater = LayoutInflater.from(context);
        this.recipes = recipes;
        this.fromSearch = fromSearch;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.layout_grid_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RecipeSummaryIM recipe = recipes.get(position);

        holder.textView.setText(recipe.getStrMeal());
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( holder.imageView.getContext() ,DetailsActivity.class);
                String recipeId = recipe.getIdMeal();
                intent.putExtra("RECIPE_ID",recipeId);
                intent.putExtra("FROM_SEARCH",fromSearch);
                holder.imageView.getContext().startActivity(intent);
            }
        });
        if(fromSearch){
            new DownloadImageTask(holder.imageView).execute(recipe.getStrMealThumb());
        }else{
            Bitmap bm = StringToBitMap(recipe.getStrMealThumb());
            holder.imageView.setImageBitmap(bm);
        }

    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }


    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte= Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder  {
        TextView textView;
        ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text_view_content);
            imageView = itemView.findViewById(R.id.image_view);
        }
    }


    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}