package si.uni_lj.fri.pbd.miniapp3.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;

@Dao
public interface RecipeDao {

    @Query("SELECT * FROM RecipeDetails WHERE idMeal = :idMeal")
    RecipeDetails getRecipeById(String idMeal);


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(RecipeDetails recipeDetails);

    @Query("DELETE FROM RecipeDetails WHERE idMeal = :idMeal")
    void deleteRecipeById(String idMeal);

    @Query("SELECT * FROM RecipeDetails ")
    List<RecipeDetails> getAllRecipes();
}
