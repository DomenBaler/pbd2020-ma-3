package si.uni_lj.fri.pbd.miniapp3.ui.favorites;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter;
import si.uni_lj.fri.pbd.miniapp3.database.Database;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;
import si.uni_lj.fri.pbd.miniapp3.models.Mapper;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;

public class FavoritesFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private Database db;
    private Mapper mapper;
    private List<RecipeDetails> allRecipes;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favourites,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = Room.databaseBuilder(getContext(), Database.class,"RecipeDetails").allowMainThreadQueries().build();
        mapper = new Mapper();
    }

    @Override
    public void onStart() {
        super.onStart();
        getDataFromDatabase();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        db.close();
    }
    //get data from database
    public void getDataFromDatabase(){
        allRecipes = db.recipeDao().getAllRecipes();

        if(allRecipes == null || allRecipes.isEmpty()){
            Toast.makeText(getContext(),"No favourites",Toast.LENGTH_LONG).show();
            if(recyclerView != null){
                recyclerView.setVisibility(View.GONE);
            }
        }else{
            // map
            List<RecipeSummaryIM> allRecipeSummary = new ArrayList<RecipeSummaryIM>();
            for(int i = 0; i < allRecipes.size(); i++){
                RecipeSummaryIM rsIM = mapper.mapRecipeDetailsToRecipeSummaryIm(allRecipes.get(i));
                allRecipeSummary.add(rsIM);
            }

            //show with RecyclerView
            recyclerView = (RecyclerView) getView().findViewById(R.id.recycler_view_favourite);
            recyclerView.setVisibility(View.VISIBLE);
            layoutManager = new LinearLayoutManager(getContext());

            recyclerView.setLayoutManager(layoutManager);
            recyclerViewAdapter = new RecyclerViewAdapter(getContext(),allRecipeSummary,false);
            recyclerView.setAdapter(recyclerViewAdapter);


        }
    }


}
