package si.uni_lj.fri.pbd.miniapp3.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.database.Database;
import si.uni_lj.fri.pbd.miniapp3.database.dao.RecipeDao;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;
import si.uni_lj.fri.pbd.miniapp3.models.Mapper;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeDetailsIM;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeDetailsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIdDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI;
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator;

public class DetailsActivity extends AppCompatActivity {
    private RestAPI mRestClient;
    private TextView tvMealName, tvMealArea, tvIngredients, tvMeasurements, tvInstructions;
    private ImageView imageView;
    private Button buttonFav;
    private boolean fromSearch;
    private boolean isFavourite;
    private String recipeId;
    private Database db;
    private Mapper mapper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        mRestClient = ServiceGenerator.createService(RestAPI.class);
        Intent intent = getIntent();
        recipeId = intent.getStringExtra("RECIPE_ID");
        fromSearch = intent.getBooleanExtra("FROM_SEARCH",true);

        db = Room.databaseBuilder(getApplicationContext(),Database.class,"RecipeDetails").allowMainThreadQueries().build();
        mapper = new Mapper();


        if(fromSearch){
            isFavourite = false;
            getRecipeDetailsRest(recipeId);
        }else{
            isFavourite = true;
            getRecipeDetailsDatabase();
        }

    }

    //close database
    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.close();
    }

    // get recipe details from the database
    private void getRecipeDetailsDatabase(){
        RecipeDetails rDetails = db.recipeDao().getRecipeById(recipeId);
        if(rDetails == null){
            Toast.makeText(getApplicationContext(),"No Favourites",Toast.LENGTH_LONG).show();
            return;

        }
        RecipeDetailsIM rDetailsIm = mapper.mapRecipeDetailsToRecipeDetailsIm(true,rDetails);

        handleUI(rDetailsIm,rDetails);
    }

    // get recipe details from the web
    private void getRecipeDetailsRest(String recipeId){
        mRestClient.getRecipeById(recipeId).enqueue(new Callback<RecipesByIdDTO>() {
            @Override
            public void onResponse(Call<RecipesByIdDTO> call, Response<RecipesByIdDTO> response) {
                if(response.isSuccessful()){
                    Log.d("response","successful");
                    RecipeDetailsDTO rd = response.body().getRecipeDetails().get(0);
                    RecipeDetailsIM rdIm= mapper.mapRecipeDetailsDtoToRecipeDetailsIm(false,rd);
                    RecipeDetails rDetails = mapper.mapRecipeDetailsDtoToRecipeDetails(false,rd);

                    handleUI(rdIm,rDetails);
                }else{
                    Log.d("response","not successful");
                }
            }

            @Override
            public void onFailure(Call<RecipesByIdDTO> call, Throwable t) {
                Log.d("response","failure");
                Toast.makeText(getApplicationContext(),"Check internet connection",Toast.LENGTH_LONG).show();
            }
        });

    }
    // change UI
    private void handleUI(RecipeDetailsIM rd,RecipeDetails rDetails){
        tvMealName = (TextView) findViewById(R.id.textView_meal_name);
        tvMealArea = (TextView) findViewById(R.id.textView_meal_area);
        tvIngredients = (TextView) findViewById(R.id.textView_ingredients_list);
        tvMeasurements = (TextView) findViewById(R.id.textView_measurements_list);
        tvInstructions = (TextView) findViewById(R.id.textView_instructions);

        buttonFav = (Button) findViewById(R.id.button_favourite);
        imageView = (ImageView) findViewById(R.id.imageView_meal);

        tvMealName.setText(rd.getStrMeal());
        tvMealArea.setText(rd.getStrArea());
        tvInstructions.setText(rd.getStrInstructions());

        if(isFavourite){
            buttonFav.setText(" REMOVE ");
        }else{
            buttonFav.setText("FAVORITE");
        }

        buttonFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFavourite = !isFavourite;
                if(isFavourite){
                    buttonFav.setText(" REMOVE ");

                    // delete old recipe and add new (in case it is updated)
                    // convert drawable to String and save to strMealThumb
                    db.recipeDao().deleteRecipeById(rd.getIdMeal());
                    BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
                    Bitmap bitmap = drawable.getBitmap();
                    rDetails.setStrMealThumb(BitMapToString(bitmap));
                    db.recipeDao().insert(rDetails);


                }else{
                    buttonFav.setText("FAVORITE");
                    db.recipeDao().deleteRecipeById(rd.getIdMeal());
                }
            }
        });

        if(!isFavourite){
            //download image
            new DownloadImageTask(imageView).execute(rd.getStrMealThumb());
        }else{
            //get image from strMealThumb
            Bitmap bm = StringToBitMap(rDetails.getStrMealThumb());
            imageView.setImageBitmap(bm);

        }

        //set ingredients and measurements

        String ingredients = rd.getStrIngredient1();
        String tmp = rd.getStrIngredient2();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient3();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient4();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient5();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient6();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient7();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient8();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient9();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient10();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient11();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }
        tmp = rd.getStrIngredient12();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient13();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient14();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient15();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient16();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient17();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient18();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient19();
        if(tmp != null && !tmp.equals("") && !tmp.equals(" ")){
            ingredients = ingredients+", "+tmp;
        }

        tmp = rd.getStrIngredient20();
        if(tmp != null && !tmp.equals("")){
            ingredients = ingredients+", "+tmp;
        }

        tvIngredients.setText(ingredients);


        String measurements = rd.getStrMeasure1();

        String tmpM = rd.getStrMeasure2();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure3();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure4();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure5();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure6();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure7();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure8();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure9();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure10();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure11();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure12();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure13();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure14();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure15();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure16();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure17();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure18();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure19();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tmpM = rd.getStrMeasure20();
        if(tmpM != null && !tmpM.equals("") && !tmpM.equals(" ")){
            measurements = measurements+", "+tmpM;
        }

        tvMeasurements.setText(measurements);

    }
// bitmap to string
    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
// String to bitmap
    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }
// download image
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
