package si.uni_lj.fri.pbd.miniapp3.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO;

public class SpinnerAdapter extends BaseAdapter {

    private Context context;
    private List<IngredientDTO> ingredients;


    public SpinnerAdapter(Context context, List<IngredientDTO> ingredients){
        this.context = context;
        this.ingredients = ingredients;
    }

    @Override
    public int getCount() {
        return ingredients.size();
    }

    @Override
    public Object getItem(int position) {
        return ingredients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.spinner_item, parent, false);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.text_view_spinner_item);
        tv.setText(ingredients.get(position).getStrIngredient());

        return convertView;
    }
}
