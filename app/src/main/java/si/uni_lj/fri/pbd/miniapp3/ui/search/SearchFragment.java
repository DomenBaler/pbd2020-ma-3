package si.uni_lj.fri.pbd.miniapp3.ui.search;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter;
import si.uni_lj.fri.pbd.miniapp3.adapter.SpinnerAdapter;
import si.uni_lj.fri.pbd.miniapp3.models.Mapper;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeSummaryDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI;
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator;

public class SearchFragment extends Fragment {

    private SpinnerAdapter spinnerAdapter;
    private Spinner spinner;
    private RestAPI mRestClient;
    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private Mapper mapper;
    private String backupIngredient;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search,container,false);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRestClient = ServiceGenerator.createService(RestAPI.class);
        mapper = new Mapper();
        backupIngredient = "chicken";

        getIngredients();
    }

    @Override
    public void onStart() {
        super.onStart();
        // set up swipe refresh
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRecipes(backupIngredient);
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }
// get all ingredients
    private void getIngredients(){
        mRestClient.getAllIngredients().enqueue(new Callback<IngredientsDTO>() {
            @Override
            public void onResponse(Call<IngredientsDTO> call, Response<IngredientsDTO> response) {
                if(response.isSuccessful()){
                    Log.d("response",response.toString());
                    List<IngredientDTO> allIngredients = response.body().getIngredients();
                    populateSpinner(allIngredients);
                }else{
                    Log.d("response","not successful");
                }
            }

            @Override
            public void onFailure(Call<IngredientsDTO> call, Throwable t) {
                Log.d("response","failure");
                Toast.makeText( getContext(), "please check your internet connection", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void populateSpinner(List<IngredientDTO> allIngredients ){
        spinner = (Spinner) getActivity().findViewById(R.id.spinner);
        spinnerAdapter = new SpinnerAdapter(getContext(),allIngredients);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                IngredientDTO ing = (IngredientDTO )parent.getSelectedItem();
                backupIngredient = ing.getStrIngredient();
                getRecipes(ing.getStrIngredient());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
// get list of recipes
    private void getRecipes(String ingredient){
        mRestClient.getRecipes(ingredient).enqueue(new Callback<RecipesByIngredientDTO>() {
            @Override
            public void onResponse(Call<RecipesByIngredientDTO> call, Response<RecipesByIngredientDTO> response) {
                Log.d("response","successful");
                Log.d("response",response.toString());

                if(response.isSuccessful()){
                    List<RecipeSummaryDTO> tmpRecipes = response.body().getRecipes();
                    if(tmpRecipes != null && !tmpRecipes.isEmpty()){
                        populateRecyclerView(tmpRecipes);
                    }else{
                        Toast.makeText(getContext(),"There are no recipes with this ingredient",Toast.LENGTH_LONG).show();
                        spinner.setSelection(0);
                    }

                }else{

                }
            }

            @Override
            public void onFailure(Call<RecipesByIngredientDTO> call, Throwable t) {
                Log.d("response","not successful");
                Toast.makeText( getContext(), "please check your internet connection", Toast.LENGTH_LONG).show();
            }
        });
    }
// fill recycler view
    private void populateRecyclerView(List<RecipeSummaryDTO> tmpRecipes){
        recyclerView = (RecyclerView) getView().findViewById(R.id.recycler_view_search);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        List<RecipeSummaryIM> allRecipeSummary = new ArrayList<RecipeSummaryIM>();
        for(int i = 0; i < tmpRecipes.size(); i++){
            RecipeSummaryIM rsIM = mapper.mapRecipeSummaryDtoToRecipeSummaryIm(tmpRecipes.get(i));
            allRecipeSummary.add(rsIM);
        }


        recyclerViewAdapter = new RecyclerViewAdapter(getContext(),allRecipeSummary,true);
        recyclerView.setAdapter(recyclerViewAdapter);
    }


}
